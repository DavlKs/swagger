create sequence if not exists location_seq start 1;
create sequence if not exists department_seq start 1;
create sequence if not exists employee_seq start 1;


CREATE TABLE locations
(
    location_id int not null default nextval('location_seq') primary key,
    street_address VARCHAR(40),
    city VARCHAR(40)
);

CREATE TABLE jobs
(
    job_id VARCHAR(10) primary key,
    job_title VARCHAR(35),
    min_salary INTEGER,
    max_salary INTEGER
);


CREATE TABLE departments
(
    department_id int not null default nextval('department_seq') primary key ,
    department_name VARCHAR(30),
    manager_id INTEGER,
    location_id INTEGER references locations(location_id)
);


CREATE TABLE employees
(
    employee_id int not null default nextval('employee_seq') primary key,
    first_name VARCHAR(20),
    last_name VARCHAR(25),
    email VARCHAR(25),
    phone_number VARCHAR(20),
    hire_date DATE,
    job_id VARCHAR(10) references jobs(job_id),
    salary INTEGER,
    manager_id INTEGER references employees(employee_id),
    department_id INTEGER references departments(department_id)
);

CREATE TABLE job_history
(
    employee_id INTEGER NOT NULL references employees(employee_id),
    start_date DATE check (start_date < job_history.end_date),
    end_date DATE check (end_date > job_history.start_date),
    job_id VARCHAR(10) references jobs(job_id),
    department_id INTEGER references departments(department_id),
    PRIMARY KEY (employee_id, start_date)
);

ALTER TABLE departments ADD FOREIGN KEY (manager_id) references employees(employee_id);

ALTER TABLE employees ADD CONSTRAINT email_unique UNIQUE (email);

ALTER TABLE locations ALTER COLUMN city SET NOT NULL;
ALTER TABLE departments ALTER COLUMN department_name SET NOT NULL;
ALTER TABLE jobs ALTER COLUMN job_title SET NOT NULL;
ALTER TABLE employees ALTER COLUMN last_name SET NOT NULL;
ALTER TABLE employees ALTER COLUMN email SET NOT NULL;
ALTER TABLE employees ALTER COLUMN hire_date SET NOT NULL;
ALTER TABLE job_history ALTER COLUMN job_id SET NOT NULL;

