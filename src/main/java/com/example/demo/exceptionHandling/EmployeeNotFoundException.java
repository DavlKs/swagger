package com.example.demo.exceptionHandling;

public class EmployeeNotFoundException extends RuntimeException {


    public EmployeeNotFoundException(String message) {
        super(message);
    }
}
