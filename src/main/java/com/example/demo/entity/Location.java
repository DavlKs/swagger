package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Сущность локации
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    private int locationId;
    private String streetAddress;
    private String City;


}
