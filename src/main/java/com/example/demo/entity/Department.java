package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Сущность отдела
 */



@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department {

    private int departmentId;
    private String departmentName;
    private int managerId;
    private int locationId;

}
