package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Сущность записи о предыдущих должностях сотрудника
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobHistory {

    private int employeeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String jobId;
    private int departmentId;
}
