package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Сущность должности
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {

    private String jobId;
    private String jobTitle;
    private int minSalary;
    private int maxSalary;

}
