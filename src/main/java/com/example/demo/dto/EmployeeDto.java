package com.example.demo.dto;

import com.example.demo.entity.Department;
import com.example.demo.entity.Employee;
import com.example.demo.entity.Job;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.time.LocalDate;

/**
 * Модель данных сотрудника
 */


@Data
@Schema(title = "Сущность сотрудника")
public class EmployeeDto {

    @Schema(title = "Идентификатор сотрудника")
    private int employeeId;
    @Schema(title = "Имя сотрудника")
    private String firstName;
    @Schema(title = "Фамилия сотрудника", required = true)
    private String lastName;
    @Schema(title = "Электронная почта", required = true)
    private String email;
    @Schema(title = "Номер телефона")
    private String phoneNumber;
    @Schema(title = "Дата найма на работу", required = true)
    private LocalDate hireDate;
    @Schema(title = "Идентификатор работы")
    private String jobId;
    @Schema(title = "Сущность работы")
    private Job job;
    @Schema(title = "Зарплата")
    private int salary;
    @Schema(title = "Идентификатор начальника")
    private int managerId;
    @Schema(title = "Сущность начальника")
    private Employee manager;
    @Schema(title = "Идентфикатор отдела")
    private int departmentId;
    @Schema(title = "Сущность отдела")
    private Department department;

}
