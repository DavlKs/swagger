package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Модель данных локации
 */


@Data
@Schema(title = "Сущность локации")
public class LocationDto {

    @Schema(title = "Идентификатор локации")
    private Integer locationId;

    @Schema(title = "Уличный адрес")
    private String streetAddress;

    @Schema(title = "Город", required = true)
    private String city;
}
