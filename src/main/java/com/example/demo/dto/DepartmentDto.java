package com.example.demo.dto;

import com.example.demo.entity.Location;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Модель данных отдела
 */

@Data
@Schema(title = "Модель данных отдела")
public class DepartmentDto {

    @Schema(title = "Идентификатор отдела")
    private int departmentId;
    @Schema(title = "Название отдела", required = true)
    private String departmentName;
    @Schema(title = "Менеджер отдела")
    private int managerId;
    @Schema(title = "Имя менеджера отдела")
    private String managerFirstName;
    @Schema(title = "Фамилия менеджера отдела")
    private String managerLastName;
    @Schema(title = "Идентификатор локации")
    private int locationId;
    @Schema(title = "Сущность локации")
    private Location location;

}
