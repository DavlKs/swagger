package com.example.demo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * Модель данных должности
 */


@Data
@Schema(title = "Сущность должности")
public class JobDto {

    @Schema(title = "Идентификатор должности")
    private String jobId;
    @Schema(title = "Название должности", required = true)
    private String jobTitle;
    @Schema(title = "Минимальная зарплата")
    private int minSalary;
    @Schema(title = "Максимальная зарплата")
    private int maxSalary;

}
