package com.example.demo.security.service.api;


import com.example.demo.security.model.LoginRequestDTO;
import com.example.demo.security.model.RegistrationRequestDTO;
import com.example.demo.security.model.User;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {

    Authentication authorize(LoginRequestDTO loginRequestDTO);

    User register(RegistrationRequestDTO registrationRequestDTO);

    void logout();

}
