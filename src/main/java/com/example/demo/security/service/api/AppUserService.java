package com.example.demo.security.service.api;

import com.example.demo.security.model.User;

import java.util.List;

public interface AppUserService {

    List<User> getAllUsers();

    User getUserById(int userId);

    void addUser(User user);

    void updateUser(User user);

    void deleteUser(int userId);
}
