package com.example.demo.dao;

import com.example.demo.entity.JobHistory;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Класс для связи базы данных и записей о предыдущих должностях сотрудников
 */

@Mapper
@Repository
public interface JobHistoryMapper {

    @Insert("insert into job_history values (#{employeeId}, #{startDate}, #{endDate}, " +
            "#{jobId}, #{departmentId})")
    void addJobHistory(JobHistory jobHistory);
}
