package com.example.demo.dao;

import com.example.demo.entity.Employee;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Класс для связи базы данных и сервиса по работе с работниками
 */

@Mapper
@Repository
public interface EmployeeMapper {

    @Select("select * from employees")
    List<Employee> getAllEmployees();

    @Select("select * from employees where employee_id = #{id}")
    Optional<Employee> getEmployeeById(int id);

    @Insert("insert into employees " +
            "values (#{employeeId}, #{firstName}, #{lastName}, #{email}, #{phoneNumber}, #{hireDate}, " +
            "#{jobId}, #{salary}, #{managerId}, #{departmentId} )")
    @SelectKey(keyProperty = "employeeId", before = true, resultType = Integer.class,
            statement = "select nextval('employee_seq')")
    void addEmployee(Employee employee);

    @Delete("delete from employees where employee_id = #{id}")
    void deleteEmployee(int id);

    @Update("update employees set first_name = #{firstName}, last_name = #{lastName}, " +
            "email = #{email}, phone_number = #{phoneNumber}, hire_date = #{hireDate}, " +
            "job_id = #{jobId}, salary = #{salary}, manager_id = #{managerId}, " +
            "department_id = #{departmentId} where employee_id = #{employeeId}")
    void updateEmployee(Employee employee);

    @Select("select * from employees where employee_id  = #{id}")
    Optional<Employee> getExpandedEmployee(int id);

    @Select("select * from employees where last_name = #{lastName}")
    List<Employee> getEmployeesByLastName(String lastName);

    @Select("select * from employees where first_name = #{firstName}")
    List<Employee> getEmployeesByFirstName(String firstName);

    @Select("select * from employees where email = #{email}")
    List<Employee> getEmployeesByEmail(String email);

    @Select("select * from employees where hire_date BETWEEN #{from} AND #{till}")
    List<Employee> getEmployeesByHireDateRange(LocalDate from, LocalDate till);





}
