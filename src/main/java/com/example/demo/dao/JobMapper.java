package com.example.demo.dao;

import com.example.demo.entity.Job;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Класс для связи базы данных и должностей
 */

@Mapper
@Repository
public interface JobMapper {

    @Select("select * from jobs where job_id = #{jobId}")
    Job getJobById(String jobId);

    @Insert("insert into jobs (job_id, job_title, min_salary, max_salary) values " +
            "(#{jobId}, #{jobTitle}, #{minSalary}, #{maxSalary})")
    void addJob(Job job);
}
