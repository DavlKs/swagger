package com.example.demo.dao;

import com.example.demo.entity.Location;
import org.apache.ibatis.annotations.*;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Класс для связи базы данных и сервиса по работе с локациями
 */

@Repository
@Mapper
public interface LocationMapper {

    @Select("select * from locations")
    List<Location> getAllLocations();

    @Select("select * from locations where location_id = #{id}")
    Optional<Location> getLocationById(int id);

    @Insert("insert into locations  values (#{locationId}, #{streetAddress}, #{city})")
    @SelectKey(keyProperty = "locationId", before = true, resultType = Integer.class,
            statement = "select nextval('location_seq')")
    void addLocation(Location location);

    @Delete("delete from locations where location_id = #{id}")
    void deleteLocation(int id);
}
