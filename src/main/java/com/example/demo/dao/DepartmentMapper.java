package com.example.demo.dao;

import com.example.demo.dto.DepartmentDto;
import com.example.demo.entity.Department;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

/**
 * Класс для связи базы данных и сервиса по работе с отделами
 */

@Mapper
@Repository
public interface DepartmentMapper {

    @Select("select * from departments")
    List<Department> getAllDepartments();

    @Select("select * from departments where department_id = #{id}")
    Optional<Department> getDepartmentById(int id);

    @Insert("insert into departments values (#{departmentId}, #{departmentName}, #{managerId}, #{locationId})")
    @SelectKey(keyProperty = "departmentId", before = true, resultType = Integer.class,
            statement = "select nextval('department_seq')")
    void addDepartment(Department department);

    @Delete("delete from departments where department_id = #{id}")
    void deleteDepartment(int id);

    @Update("update departments set department_name = #{departmentName}, " +
            "manager_id = #{managerId}, location_id = #{locationId} where " +
            "department_id = #{departmentId}")
    void updateDepartment(Department department);

    @Select("select d.department_id, department_name, location_id, d.manager_id, " +
            "first_name as managerFirstName, last_name as managerLastName \n" +
            "       from departments d\n" +
            "join employees e on d.manager_id = e.employee_id\n" +
            "where e.department_id = #{id}")
    Optional<DepartmentDto> getDepartmentWithManager(int id);

    @Select("select * from departments where department_name = #{departmentName}")
    List<Department> getDepartmentByName(String departmentName);

    @Select("select * from departments " +
            "join employees on departments.manager_id = employee_id  where last_name = #{lastName}")
    List<Department> getDepartmentByManagerLastName(String lastName);
}
