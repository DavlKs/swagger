package com.example.demo.controller;

import com.example.demo.dto.LocationDto;
import com.example.demo.service.api.LocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * REST контроллер для работы с локациями
 */

@RestController
@Tag(name = "Локация", description = "API для локаций")
@RequestMapping("/location")
public class LocationController {

    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping
    @Operation(summary = "Получение всех локаций")
    public List<LocationDto> getAllLocations() {
        return locationService.getAllLocations();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение локации по идентификатору")
    public LocationDto getLocationById(@PathVariable int id) {
        return locationService.getLocationById(id);
    }

    @PostMapping
    @Operation(summary = "Добавление новой локации")
    public LocationDto addLocation(@RequestBody LocationDto location) {
        return locationService.addLocation(location);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление локации")
    public void deleteLocation(@PathVariable int id) {
        locationService.deleteLocation(id);
    }


}
