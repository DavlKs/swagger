package com.example.demo.controller;

import com.example.demo.dto.DepartmentDto;
import com.example.demo.exceptionHandling.DepartmentNotFoundDto;
import com.example.demo.exceptionHandling.DepartmentNotFoundException;
import com.example.demo.service.api.DepartmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
 REST контроллер для работы с отделами
*/

@Tag(name = "Отдел", description = "API для отделов")
@RestController
@RequestMapping("/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping
    @Operation(summary = "Получение всех отделов")
    public List<DepartmentDto> getAllDepartments() {
        return departmentService.getAllDepartments();
    }

    @GetMapping("/{id}")
    @Operation (summary = "Получение отдела по идентификатору")
    public DepartmentDto getDepartmentById(@PathVariable int id) {
        return departmentService.getDepartmentById(id);
    }

    @GetMapping("/filter")
    @Operation(summary = "Получение отдела по заданному полю")
    public List<DepartmentDto> getDepartmentByFilter(@RequestParam String searchField, @RequestParam String searchValue) {
        return departmentService.getDepartmentByFilter(searchField, searchValue);
    }

    @GetMapping("/id/{id}")
    @Operation(summary = "Получение отдела с именем менеджера и локацией")
    public DepartmentDto getDepartmentWithManagerAndLocation(@PathVariable int id) {
        return departmentService.getDepartmentWithManagerAndLocation(id);
    }

    @PostMapping
    @Operation(summary = "Добавление нового отдела")
    public DepartmentDto addDepartment(@RequestBody DepartmentDto departmentDto) {
        return departmentService.addDepartment(departmentDto);
    }

    @PutMapping
    @Operation(summary = "Изменение отдела")
    public DepartmentDto updateDepartment(@RequestBody DepartmentDto departmentDto) {
        return departmentService.updateDepartment(departmentDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление отдела")
    public void deleteDepartment(@PathVariable int id) {
        departmentService.deleteDepartment(id);
    }

    @ExceptionHandler
    public ResponseEntity<DepartmentNotFoundDto> handleException(DepartmentNotFoundException exception) {
        DepartmentNotFoundDto dto = new DepartmentNotFoundDto();
        dto.setInfo(exception.getMessage());
        return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
    }

}
