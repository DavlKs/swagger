package com.example.demo.controller;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.exceptionHandling.EmployeeNotFoundDto;
import com.example.demo.exceptionHandling.EmployeeNotFoundException;
import com.example.demo.service.api.EmployeeService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

/**
 * REST контроллер для работы с сотрудниками
 */

@RestController
@RequestMapping("/employee")
@Tag(name = "Сотрудник", description = "API для сотрудников")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    @Operation(summary = "Получение всех сотрудников")
    public List<EmployeeDto> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @PostMapping
    @Operation(summary = "Добавление нового сотрудника")
    public EmployeeDto addEmployee(@RequestBody EmployeeDto employeeDto) {
        return employeeService.addEmployee(employeeDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление сотрудника")
    public void deleteEmployee(@PathVariable int id) {
        employeeService.deleteEmployee(id);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение сотрудника по идентификатору со всеми связанными сущностями")
    public EmployeeDto getExpandedEmployee(@PathVariable int id) {
        return employeeService.getExpandedEmployee(id);
    }

    @GetMapping("/lastname/{lastName}")
    @Operation(summary = "Получение списка сотрудников с заданной фамилией")
    public List<EmployeeDto> getEmployeesByLastName(@PathVariable String lastName) {
        return employeeService.getEmployeesByLastName(lastName);
    }

    @GetMapping("/firstname/{firstName}")
    @Operation(summary = "Получение списка сотрудников с заданным именем")
    public List<EmployeeDto> getEmployeesByFirstName(@PathVariable String firstName) {
        return employeeService.getEmployeesByFirstName(firstName);
    }

    @GetMapping("/email/{email}")
    @Operation(summary = "Получение списка сотрудников с заданной электронной почтой")
    public List<EmployeeDto> getEmployeesByEmail(@PathVariable String email) {
        return employeeService.getEmployeesByEmail(email);
    }

    @GetMapping("/hiredate/{from}/{till}")
    @Operation(summary = "Получение списка сотрудников, нанятых в диапазоне заданных дат")
    public List<EmployeeDto> getEmployeesByHireDateRange
            (@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
             @PathVariable @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate till) {
        return employeeService.getEmployeesByHireDateRange(from, till);
    }

    @PutMapping
    @Operation(summary = "Изменение сотрудника")
    public EmployeeDto updateEmployee(@RequestBody EmployeeDto employeeDto) {
        return employeeService.updateEmployee(employeeDto);
    }

    @ExceptionHandler
    public ResponseEntity<EmployeeNotFoundDto> handleException(EmployeeNotFoundException exception) {
        EmployeeNotFoundDto dto = new EmployeeNotFoundDto();
        dto.setInfo(exception.getMessage());
        return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
    }



}
