package com.example.demo.service.impl;

import com.example.demo.dao.LocationMapper;
import com.example.demo.dto.LocationDto;
import com.example.demo.entity.Location;
import com.example.demo.service.api.LocationService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Имплементация сервиса по работе с локациями
 */

@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

    private final LocationMapper locationMapper;
    private final ModelMapper modelMapper;

    public LocationServiceImpl(LocationMapper locationMapper, ModelMapper modelMapper) {
        this.locationMapper = locationMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<LocationDto> getAllLocations() {

        List<Location> locationEntities = locationMapper.getAllLocations();
        return modelMapper.map(locationEntities, TypeToken.of(List.class).getType());
    }

    @Override
    public LocationDto getLocationById(int id) {
        Location locationEntity = locationMapper.getLocationById(id).orElseThrow(
                () -> new RuntimeException
                        (String.format("Не найдена сущность локации по идентификатору=%s", id))
                );
        log.error("Получена сущность с идентификатором: {}", id);
        return modelMapper.map(locationEntity, LocationDto.class);
    }

    @Override
    public LocationDto addLocation(LocationDto locationDto) {
            Location location = modelMapper.map(locationDto, Location.class);
        locationMapper.addLocation(location);
        log.info("Создана сущность с id = {}", location.getLocationId());
        return getLocationById(location.getLocationId());
    }

    @Override
    public void deleteLocation(int id) {
        locationMapper.deleteLocation(id);
        log.error("Удалена сущность с id = {}", id);
    }
}
