package com.example.demo.service.impl;

import com.example.demo.dao.DepartmentMapper;
import com.example.demo.dao.EmployeeMapper;
import com.example.demo.dao.LocationMapper;
import com.example.demo.dto.DepartmentDto;
import com.example.demo.entity.Department;
import com.example.demo.entity.Employee;
import com.example.demo.entity.Location;
import com.example.demo.exceptionHandling.DepartmentNotFoundException;
import com.example.demo.exceptionHandling.EmployeeNotFoundException;
import com.example.demo.service.api.DepartmentService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Имплементация сервиса по работе с отделами
 */

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private static final String DEPARTMENT_NAME = "department_name";
    private static final String MANAGER_LASTNAME = "last_name";

    private final DepartmentMapper departmentMapper;
    private final ModelMapper modelMapper;
    private final LocationMapper locationMapper;
    private final EmployeeMapper employeeMapper;

    public DepartmentServiceImpl(DepartmentMapper departmentMapper, ModelMapper modelMapper, LocationMapper locationMapper, EmployeeMapper employeeMapper) {
        this.departmentMapper = departmentMapper;
        this.modelMapper = modelMapper;
        this.locationMapper = locationMapper;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public List<DepartmentDto> getAllDepartments() {
        List <Department> departments = departmentMapper.getAllDepartments();
        return modelMapper.map(departments, TypeToken.of(List.class).getType());
    }

    @Override
    public DepartmentDto getDepartmentById(int id) {
        Department entity = departmentMapper.getDepartmentById(id)
                .orElseThrow(() -> new DepartmentNotFoundException("Не найдена сущность отдела с идентификатором = " + id));
        return modelMapper.map(entity, DepartmentDto.class);
    }

    @Override
    public DepartmentDto addDepartment(DepartmentDto departmentDto) {
        Department entity = modelMapper.map(departmentDto, Department.class);
        departmentMapper.addDepartment(entity);
        return getDepartmentById(entity.getDepartmentId());
    }

    @Override
    public void deleteDepartment(int id) {
        departmentMapper.deleteDepartment(id);
    }

    @Override
    public DepartmentDto getDepartmentWithManagerAndLocation(int id) {
        Department department = departmentMapper.getDepartmentById(id)
                .orElseThrow(() -> new DepartmentNotFoundException("Не найдена сущность отдела с идентификатором = " + id));
        int locationId = department.getLocationId();
        Employee employee = employeeMapper.getEmployeeById(department.getManagerId()).get();
        Location location = locationMapper.getLocationById(locationId).get();
        String managerFirstName =employee.getFirstName();
        String managerLastName = employee.getLastName();

        DepartmentDto departmentDto = modelMapper.map(department, DepartmentDto.class);
        departmentDto.setLocation(location);
        departmentDto.setManagerFirstName(managerFirstName);
        departmentDto.setManagerLastName(managerLastName);
        return departmentDto;
    }

    @Override
    public DepartmentDto updateDepartment(DepartmentDto departmentDto) {
        Department department = modelMapper.map(departmentDto, Department.class);
        departmentMapper.updateDepartment(department);
        return getDepartmentById(department.getDepartmentId());
    }

    @Override
    public List<DepartmentDto> getDepartmentByFilter(String searchField, String searchValue) {
        List<Department> departments = null;
        if (searchField.equals(DEPARTMENT_NAME)) {
            departments = departmentMapper.getDepartmentByName(searchValue);
        } else if (searchField.equals(MANAGER_LASTNAME)) {
            departments = departmentMapper.getDepartmentByManagerLastName(searchValue);
        } else {
            throw new IllegalArgumentException("Поиск департамента по заданному полю невозможен");
        }
        return modelMapper.map(departments, TypeToken.of(List.class).getType());
    }
}
