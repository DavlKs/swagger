package com.example.demo.service.impl;

import com.example.demo.dao.DepartmentMapper;
import com.example.demo.dao.EmployeeMapper;
import com.example.demo.dao.JobHistoryMapper;
import com.example.demo.dao.JobMapper;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Department;
import com.example.demo.entity.Employee;
import com.example.demo.entity.Job;
import com.example.demo.entity.JobHistory;
import com.example.demo.exceptionHandling.EmployeeNotFoundException;
import com.example.demo.service.api.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Имплементация сервиса по работе с сотрудниками
 */

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeMapper employeeMapper;
    private final ModelMapper modelMapper;
    private final DepartmentMapper departmentMapper;
    private final JobMapper jobMapper;
    private final JobHistoryMapper jobHistoryMapper;

    public EmployeeServiceImpl(EmployeeMapper employeeMapper, ModelMapper modelMapper, DepartmentMapper departmentMapper, JobMapper jobMapper, JobHistoryMapper jobHistoryMapper) {
        this.employeeMapper = employeeMapper;
        this.modelMapper = modelMapper;
        this.departmentMapper = departmentMapper;
        this.jobMapper = jobMapper;
        this.jobHistoryMapper = jobHistoryMapper;
    }

    @Override
    public List<EmployeeDto> getAllEmployees() {
        List <Employee> entities = employeeMapper.getAllEmployees();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public EmployeeDto getEmployeeById(int id) {
        Employee entity = employeeMapper.getEmployeeById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Не найдена сущность сотрудника с идентификатором = " + id));
        return modelMapper.map(entity, EmployeeDto.class);
    }

    @Override
    public EmployeeDto addEmployee(EmployeeDto employeeDto) {

        String jobId = employeeDto.getJobId();

        if (jobId == null) {
            Job job = employeeDto.getJob();
            jobMapper.addJob(job);
            employeeDto.setJobId(job.getJobId());
        } else {
            Job jobById = jobMapper.getJobById(jobId);
            if (jobById == null) {
                throw new RuntimeException("Не найдена сущность работы с идентификатором = " + jobId);
            }
        }

        Employee entity = modelMapper.map(employeeDto, Employee.class);

        employeeMapper.addEmployee(entity);

        return getEmployeeById(entity.getEmployeeId());

    }

    @Override
    public void deleteEmployee(int id) {
        employeeMapper.deleteEmployee(id);
    }

    @Override
    public EmployeeDto updateEmployee(EmployeeDto employeeDto) {

        Employee previousEntity = employeeMapper.getEmployeeById(employeeDto.getEmployeeId())
                .orElseThrow(() -> new EmployeeNotFoundException(
                        "Не найдена сущность сотрудника с идентификатором = " + employeeDto.getEmployeeId()));

        Employee updatedEntity = modelMapper.map(employeeDto, Employee.class);

        if (!previousEntity.getJobId().equals( updatedEntity.getJobId()) ||
                previousEntity.getDepartmentId() != updatedEntity.getDepartmentId()) {

            JobHistory jobHistory = new JobHistory();
            jobHistory.setEmployeeId(previousEntity.getEmployeeId());
            jobHistory.setStartDate(previousEntity.getHireDate());
            jobHistory.setEndDate(LocalDate.now());
            jobHistory.setJobId(previousEntity.getJobId());
            jobHistory.setDepartmentId(previousEntity.getDepartmentId());
            jobHistoryMapper.addJobHistory(jobHistory);

        }

        employeeMapper.updateEmployee(updatedEntity);

        return getEmployeeById(updatedEntity.getEmployeeId());
    }

    @Override
    public EmployeeDto getExpandedEmployee(int id) {
        Employee employee = employeeMapper.getExpandedEmployee(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Не найдена сущность сотрудника с идентификатором = " + id));

        Job job = jobMapper.getJobById(employee.getJobId());

        Department department = departmentMapper.getDepartmentById(employee.getDepartmentId()).get();

        Employee manager = employeeMapper.getEmployeeById(employee.getManagerId()).get();

        EmployeeDto dto = modelMapper.map(employee, EmployeeDto.class);
        dto.setDepartment(department);
        dto.setJob(job);
        dto.setManager(manager);
        return dto;
    }

    @Override
    public List<EmployeeDto> getEmployeesByLastName(String lastName) {
        List<Employee> entities = employeeMapper.getEmployeesByLastName(lastName);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDto> getEmployeesByFirstName(String firstName) {
        List<Employee> entities = employeeMapper.getEmployeesByFirstName(firstName);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDto> getEmployeesByEmail(String email) {
        List<Employee> entities = employeeMapper.getEmployeesByEmail(email);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeeDto> getEmployeesByHireDateRange(LocalDate from, LocalDate till) {
        List<Employee> entities = employeeMapper.getEmployeesByHireDateRange(from, till);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }


}
