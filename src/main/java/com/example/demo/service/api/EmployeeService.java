package com.example.demo.service.api;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Employee;

import java.time.LocalDate;
import java.util.List;

/**
 * Сервис по работе с сотрудниками
 */

public interface EmployeeService {

    List<EmployeeDto> getAllEmployees();

    EmployeeDto getEmployeeById(int id);

    EmployeeDto addEmployee(EmployeeDto employeeDto);

    void deleteEmployee(int id);

    EmployeeDto updateEmployee(EmployeeDto employeeDto);

    EmployeeDto getExpandedEmployee(int id);

    List<EmployeeDto> getEmployeesByLastName(String lastName);

    List<EmployeeDto> getEmployeesByFirstName(String firstName);

    List<EmployeeDto> getEmployeesByEmail(String email);

    List<EmployeeDto> getEmployeesByHireDateRange(LocalDate from, LocalDate till);
}
