package com.example.demo.service.api;

import com.example.demo.dto.DepartmentDto;

import java.util.List;

/**
 * Сервис по работе с отделами
 */

public interface DepartmentService {

    List<DepartmentDto> getAllDepartments();

    DepartmentDto getDepartmentById(int id);

    DepartmentDto addDepartment(DepartmentDto departmentDto);

    void deleteDepartment(int id);

    DepartmentDto getDepartmentWithManagerAndLocation(int id);

    DepartmentDto updateDepartment(DepartmentDto departmentDto);

    List<DepartmentDto> getDepartmentByFilter(String searchField, String searchValue);
}
