package com.example.demo.service.api;

import com.example.demo.dto.LocationDto;
import com.example.demo.entity.Location;

import java.util.List;
import java.util.Optional;

/**
 * Сервис по работе с локациями
 */

public interface LocationService {

    List<LocationDto> getAllLocations();

    LocationDto getLocationById(int id);

    LocationDto addLocation(LocationDto locationDto);

    void deleteLocation(int id);



}
